package com.banque.classe;

import lombok.Getter;

public class CompteCourant extends Compte {
    @Getter
    private int decouvert;

    public CompteCourant(String proprietaire, int solde, int decouvert){
        super(proprietaire, solde);
        this.decouvert = decouvert;
    }

    public void afficher(){
        System.out.println("Le découvert autorisé du compte est de " + getDecouvert());
        super.afficher();
    }

    public boolean debiter(double somme){
        System.out.println("Débit de " + somme + " euro sur le compte de "+ getProprietaire());

        if((getSolde() - somme) > -getDecouvert()){
            setSolde(getSolde() - somme );
            getList_operation().add(String.valueOf(-somme));
            System.out.println();
            return true;
        }else{
            System.out.println("Impossible de réaliser l'opération. Le Solde Compte ne peut descendre en dessous du découvert\n");
            return false;
        }
    }
    
}
