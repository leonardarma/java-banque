package com.banque.classe;

public class CompteEpargne extends Compte {
    double taux;

    public CompteEpargne(String proprietaire, int solde, double taux){
        super(proprietaire, solde);
        this.taux = taux;
    }
}
