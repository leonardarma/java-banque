package com.banque.classe;

import java.util.*;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;


@FieldDefaults(level = AccessLevel.PRIVATE)
public abstract class Compte {
    @Getter 
    double solde;
    @Getter @Setter
    String proprietaire;

    private List<String> list_operation = new ArrayList<String>();

    public Compte(String proprietaire, double solde){
        setSolde(solde);
        setProprietaire(proprietaire);
    }
    
    public void crediter(double somme){
        System.out.println("Crédit de " + somme + " euro sur le compte de " + getProprietaire() + "\n");
        setSolde(getSolde() + somme );
        getList_operation().add(String.valueOf(somme));
    }

    public void crediter(double somme, Compte compte){
        if(compte.debiter(somme)){
            crediter(somme);
        };
    }

    public boolean debiter(double somme){
        System.out.println("Débit de " + somme + " euro sur le compte de "+ getProprietaire());

        if(getSolde() - somme > 0){
            setSolde(getSolde() - somme );
            getList_operation().add(String.valueOf(-somme));
            return true;
        }else{
            System.out.println("Impossible de réaliser l'opération. Le Solde Compte ne peut être négatif\n" );
            return false;
        }
    }

    public void debiter(double somme, Compte compte){

        if(debiter(somme)){
            compte.crediter(somme);
        }else{
            System.out.println(" Credit de " + somme + " euro sur le compte de " + getProprietaire() + " annulé\n");
        };
    }

    public void afficher(){
        System.out.println("Information du " + this.getClass().getSimpleName() + " de " + getProprietaire());
        System.out.println("Le Solde est de " + getSolde() + "Euro");
        System.out.println(list_operation + "\n");
    }

    protected void setSolde(double somme){
        this.solde = somme;
    }

    protected List<String> getList_operation(){
        return this.list_operation;
    }
}
