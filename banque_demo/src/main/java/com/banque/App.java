package com.banque;

import com.banque.classe.CompteCourant;
import com.banque.classe.CompteEpargne;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        CompteCourant ccNicolas = new CompteCourant("Nicolas",0,2000);
        CompteEpargne ceNicolas = new CompteEpargne("Nicolas", 0, 0.02);
        CompteCourant ccJeremy = new CompteCourant("Jeremy",500,0);

        ccNicolas.crediter(100);
        ccNicolas.debiter(50);
        ceNicolas.crediter(20, ccNicolas);
        ccNicolas.crediter(100);
        ccNicolas.crediter(20,ceNicolas);
        ccJeremy.debiter(500);
        ccJeremy.debiter(200, ccNicolas);
        ccNicolas.afficher();
        ceNicolas.afficher();
        ccJeremy.afficher();

    }
}
